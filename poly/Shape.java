package poly;

public class Shape {

	public double getarea() {
		return 0;
	}
}

class Rectangle extends Shape {
	private double width;
	private double length;

	public Rectangle(double w, double l) {
		this.width = w;
		this.length = l;
	}

	public double getarea() {
		return width * length;
	}
}

class circle extends Shape {
	private double radion;

	public circle(double r) {
		this.radion = r;
	}

	public double getarea() {
		return 3.14 * radion * radion;
	}

}
