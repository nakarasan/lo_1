package poly;

public class Polymorphism {
	
	public static void main(String[] args) {

		Shape[] shapes = new Shape[2];
		shapes[0] = new circle(7);
		shapes[1] = new Rectangle(4, 6);
		System.out.println("Area of circle: " + shapes[0].getarea());
		System.out.println("Area of Rectangle: " + shapes[1].getarea());
	}


}
