package access_modifiers;

public class Main {
    public static void main( String[] args ) {

        Mobile mobile = new Mobile();

        mobile.model = "Samsung J7 prime";
        mobile.display();
    }
}
