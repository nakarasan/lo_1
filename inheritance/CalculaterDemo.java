package inheritance;

public class CalculaterDemo {

	public static void main(String[] args) {

		BasicCalculator bc = new BasicCalculator();
		ScientificCalculator sc = new ScientificCalculator();

		System.out.println(bc.addNum(12, 12));
		System.out.println(sc.power(10, 3));
		System.out.println(sc.divNum(60, 4));

	}

}
