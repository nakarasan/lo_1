package inheritance;

public class ScientificCalculator extends BasicCalculator {

	public double power(int base, int exponent) {
		return Math.pow(base, exponent);
	}

	public double squarRoot(int number) {
		return Math.sqrt(number);
	}

}
