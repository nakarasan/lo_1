package absraction;

public class abstractDemo {

	public static void main(String args[]) {
		Person student = new Employee("Aji", "Female", 15);
		Person employee = new Employee("Kara", "Male", 255);
		student.work();
		employee.work();
		employee.changeName("Nakarasan");
		System.out.println(employee.toString());

	}

}
