package LO_1;

public class Static_Variable {

	static int age;
	static String name;

	static void display() {
		System.out.println("Age is: " + age);
		System.out.println("Name is: " + name);
	}

	public static void main(String args[]) {
		age = 30;
		name = "Steve";
		display();
	}

}
