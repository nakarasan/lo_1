package LO_1;

public class Instance_Variable {
	
    public String name;

    private int age;

    public Instance_Variable (String RecName)
    {
        name = RecName;
    }

    public void setAge(int RecSal)
    {
        age = RecSal;
    }

    public void printRec()
    {
        System.out.println("name : " + name ); 
        System.out.println("age :" + age); 
    }

    public static void main(String args[])
    {
    	Instance_Variable r = new Instance_Variable("Nakarasan");
        r.setAge(21);
        r.printRec();
    }

}

