package LO_1;

public class Static_Block {

	static int num1;
	static int num2;
	static {
		num1 = 548;
		num2 = 758;
	}

	public static void main(String args[]) {
		System.out.println("Value of Number 1 is = " + num1);
		System.out.println("Value of Number 2 is = " + num2);
	}

}
